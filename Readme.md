# URL Splitter


## Description

URL Definition
scheme://domain:port/path?query_string

Write a program to split a URL to parts: scheme, host, port, path and parameters.

1. The program will read the URL from command line.

2. The program will implement 2 splitting algorithms and compare their performance.

3. The first algorithm will use regular expressions.

4. The second algorithm will be based on a state machine.

5. The program will run 10000 iterations of each algorithm.

6. The program will output URL parts and runtimes of both algorithms.


## Explanation of implementation

The problem to solve looks simple, however, since I have limited time to solve it, I did some assumptions

First, the app will have 2 modes, Automatic and Manual mode. 

For Automatic, please **copy the file urls.properties to your desktop** since the app will read it from there. This file contains 1000 urls and it will print the performance of 10000 urls according to requirement 5.

For manual mode, the user has the possibility to interact with the app entering one single url every time and app will validate and process the url. Url has to have a valid format otherwise is not gonna be processed. That is it has to have at least scheme, domain and path.


## How to execute it

1 Build this project
```
gradle build 
```
2 Run the application from console

Please go to the folder build/distributions and unzip the file splitter-1.0-SNAPSHOT.zip

2.1 For automatic mode

In Linux

./bin/splitter

In Windows

cd bin

splitter.bat


2.2 For manual mode

In Linux

./bin/splitter manual

In Windows

cd bin

splitter.bat manual

3 Run the application from your IDE

In Intellij

Create 2 running configurations of type Application

For manual, in program arguments just put manual.
In Automatic, leave program arguments empty

See image Idea-running-config.png