package com.adrian.demo;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;

public class SplitterServiceTest {

    private SplitterService splitterService;

    @Before
    public void setUp(){
        splitterService = new SplitterService();
    }

    /**
     * Dummy test. In this case, is not necessary to test void methods
     * @throws Exception
     */
    @Test
    public void processUrl()  {
        splitterService.processUrl("https://app.vivifyscrum.com:8080/boardsbacklog");
    }

    @Test
    public void processStateForSchemeSuccess() throws Exception {
        String url = "https://app.vivifyscrum.com:8080/boardsbacklog";
        String newUrl = splitterService.findPatterns(Utils.Status.SCHEME, url);

        assertTrue(!url.equals(newUrl));
    }

    @Test
    public void processStateForSchemeNotSuccess() throws Exception {
        String url = "app.vivifyscrum.com:8080/boardsbacklog";
        String newUrl = splitterService.findPatterns(Utils.Status.SCHEME, url);

        assertTrue(url.equals(newUrl));
    }

    @Test
    public void processStateForDomainSuccess() throws Exception {
        String url = "https://app.vivifyscrum.com:8080/boardsbacklog";
        String newUrl = splitterService.findPatterns(Utils.Status.DOMAIN, url);

        assertTrue(!url.equals(newUrl));
    }

    @Test
    public void processStateForDomainNotSuccess() throws Exception {
        String url = "app.vivifyscrum.com-asd?asd";
        String newUrl = splitterService.findPatterns(Utils.Status.DOMAIN, url);

        assertTrue(url.equals(newUrl));
    }

    @Test
    public void processStateForPortSuccess() throws Exception {
        String url = "https://app.vivifyscrum.com:8080/boards";
        String newUrl = splitterService.findPatterns(Utils.Status.PORT, url);

        assertTrue(!url.equals(newUrl));
    }

    @Test
    public void processStateForPortNotSuccess() throws Exception {
        String url = "9090?asd";
        String newUrl = splitterService.findPatterns(Utils.Status.PORT, url);

        assertTrue(url.equals(newUrl));
    }

    @Test
    public void processStateForPathSuccess() throws Exception {
        String url = "boards?asdasd";
        String newUrl = splitterService.findPatterns(Utils.Status.PATH, url);

        assertTrue(!url.equals(newUrl));
    }

    @Test
    public void processStateForPathNotSuccess() throws Exception {
        String url = "boards/asd/asd";
        String newUrl = splitterService.findPatterns(Utils.Status.PATH, url);

        assertTrue("".equals(newUrl));
    }
}