package com.adrian.demo;

import org.junit.Before;
import org.junit.Test;

import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

public class LoadingUrlsServiceTest {

    public LoadingUrlsService loadingUrlsService;

    @Before
    public void setUp() throws Exception {
        loadingUrlsService = new LoadingUrlsService();
    }

    @Test()
    public void readResourceFromRandomPathAndGetEmptyUrls() throws Exception {
        Path path= Paths.get(System.getProperty("user.home"), "RandomPath");
        List<String> urls = loadingUrlsService.readResource(path, true);
        assertTrue(urls.isEmpty());
    }

    @Test
    public void readResourceFromResourcesAndGetCorrectUrls() throws Exception {
        List<String> urls = loadingUrlsService.readResourceFromResources();
        assertFalse(urls.isEmpty());
        assertTrue(urls.size()==1000);
    }


}