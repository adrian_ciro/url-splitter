package com.adrian.demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilsTest {

    private Utils utils;

    @Test
    public void urlWithSchemeDomainAndPathIsValid() throws Exception {
        String url = "http://google.com/search";
        boolean valid = Utils.isValidUrl(url);
        assertTrue(valid);
    }

    @Test
    public void urlWithSchemeDomainPortAndPathIsValid() throws Exception {
        String url = "http://yahoo.com:8080/search";
        boolean valid = Utils.isValidUrl(url);
        assertTrue(valid);
    }


    @Test
    public void urlWithSchemeDomainPortPathAndParamsIsValid() throws Exception {
        String url = "http://msn.com:80/search?test";
        boolean valid = Utils.isValidUrl(url);
        assertTrue(valid);
    }

    @Test
    public void urlWithoutSchemeAndWithDomainAndPathIsInvalid() throws Exception {
        String url = "google.com/search";
        boolean valid = Utils.isValidUrl(url);
        assertFalse(valid);
    }

    @Test
    public void urlWithSchemeDomainAndWithoutPathIsInvalid() throws Exception {
        String url = "http://yahoo.com";
        boolean valid = Utils.isValidUrl(url);
        assertFalse(valid);
    }


    @Test
    public void malformedUrlIsInvalid() throws Exception {
        String url = "http://msn.com:807878/search?test/sadf";
        boolean valid = Utils.isValidUrl(url);
        assertFalse(valid);
    }
}