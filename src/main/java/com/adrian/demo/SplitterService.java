package com.adrian.demo;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.regex.Matcher;

import static com.adrian.demo.Utils.REGEX_GROUP_NUMBER;

/**
 * Service for processing urls via regular expressions and via State machine
 */
public class SplitterService {

    private LocalDateTime startTime;
    private LocalDateTime endTime;

    /**
     * Process Url via both algorithms
     * @param url
     */
    public void processUrl(String url){
        processUrlViaRegularExpressions(url);
        processUrlViaStateMachine(url);
    }

    /**
     * Execution of regular expression algorithm
     * @param url
     */
    public void processUrlViaRegularExpressions(String url) {
        startTime = LocalDateTime.now();
        Matcher matcher = Utils.getMatcher(url);
        matcher.matches();
        int index=1;
        while(index <= REGEX_GROUP_NUMBER) {
            if(matcher.group(index) != null){
                System.out.println(Utils.cleanAndTrim(matcher.group(index)));
            }
            index++;
        }
        endTime = LocalDateTime.now();
        Utils.printPerformance("Regex: ", startTime, endTime);
    }

    /**
     * Execution of State machine algorithm
     * @param url
     */
    public void processUrlViaStateMachine(String url) {
        startTime = LocalDateTime.now();
        Arrays.stream(Utils.Status.values()).forEach(status->findPatterns(status, url));

        endTime = LocalDateTime.now();
        Utils.printPerformance("State: ", startTime, endTime);
        System.out.println();
    }

    /**
     * Method for processing url according to the given State
     * @param state
     * @param url
     * @return
     */
    public String findPatterns(Utils.Status state, String url){
        String newUrl;
        switch (state){
            case SCHEME:
                return getRestOfUrl(url,"://");

            case DOMAIN:
                newUrl = getRestOfUrl(url,":");
                if(url.equals(newUrl)){
                    newUrl = getRestOfUrl(url,"/");
                }
                return newUrl;

            case PORT:
                return getRestOfUrl(url,"/");

            case PATH:
                newUrl = getRestOfUrl(url,"\\?");
                return url.equals(newUrl) ? "" : newUrl;

            case PARAMS:
                return "";
        }
        return "";
    }

    /**
     * Split the url if symbol is inside the url and return the second part of url
     * Returns same String if Symbol is not found
     * @param url
     * @param symbol
     * @return
     */
    public String getRestOfUrl(String url, String symbol){
        String[] newUrl= url.split(symbol);
        return newUrl.length>1 ? newUrl[1] : newUrl[0];
    }

}
