package com.adrian.demo;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Service responsible for loading the urls.
 * The app will load the file from classpath, if NoSuchFileException is raised,
 * then it will try from desktop folder
 */
public class LoadingUrlsService {

    private static final String URLS_FILENAME ="urls.properties";

    /**
     * Read the file with urls and return String of urls
     * @param path
     * @param isDesktopPath
     * @return List of urls
     */
    public List<String> readResource(Path path, boolean isDesktopPath) {

        List<String> urls = new ArrayList<String>();
        try (Stream<String> stream = Files.lines(path, Charset.defaultCharset())) {
            stream.forEach(urls::add);
        }
        catch (NoSuchFileException e) {
            if(!isDesktopPath){
                System.out.println("Urls file not found, trying from desktop");
                return readResourceFromDesktop();
            }
            else{
                System.out.println("Urls file not found in Desktop folder, please try again");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return urls;
    }

    /**
     * Initialize path from resources folder
     */
    public List<String> readResourceFromResources() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(URLS_FILENAME).getFile());
        Path path= Paths.get(file.getPath());
        return readResource(path, false);
    }

    /**
     * Initialize path from Desktop folder
     */
    public List<String> readResourceFromDesktop() {
        Path path= Paths.get(System.getProperty("user.home"), "Desktop", URLS_FILENAME);
        return readResource(path, true);
    }

}
