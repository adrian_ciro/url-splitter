package com.adrian.demo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utils class which define the State machine status and helps with validation of URls via Regex
 */
public class Utils {

    public enum Status {SCHEME, DOMAIN, PORT, PATH, PARAMS}

    //Regex of the URL in the following format e.g scheme://domain:port/path?query_string
    public static final String COMMAND_REGEX = "(\\w+):\\/\\/(\\w+\\.\\w+|\\w+\\.\\w+\\.\\w+)(:\\d{1,4})?\\/(\\w+)(\\?\\w+)?";

    //Number of max strings that can be found in URL
    public static final int REGEX_GROUP_NUMBER = 5;


    /**
     * Get a matcher with the given url
     * @param url
     * @return Matcher
     */
    public static Matcher getMatcher(String url) {
        return Pattern.compile(COMMAND_REGEX).matcher(url);
    }

    /**
     * Validation of Url according to Regex pattern
     * @param command
     * @return
     */
    public static boolean isValidUrl(String command) {
        Matcher matcher = Pattern.compile(COMMAND_REGEX).matcher(command);
        return matcher.matches();
    }

    /**
     * Remove special characters and white spaces
     * @param output
     * @return
     */
    public static String cleanAndTrim(String output) {
        return output.replace(':',' ').trim().replace('?',' ').trim();
    }

    /**
     * Print the duration in msec of the given start and end times
     * @param algorithm
     * @param startTime
     * @param endTime
     */
    public static void printPerformance(String algorithm, LocalDateTime startTime, LocalDateTime endTime) {
        System.out.println(algorithm + Duration.between(startTime,endTime).getNano()/1000000 + "msec");
    }

}
