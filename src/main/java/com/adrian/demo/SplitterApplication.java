package com.adrian.demo;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Scanner;

/**
 * The start of the application depends how the user wants to interacts. There are 2 modes:
 * Manual mode: User enter one url and app will validate and process
 * Automatic mode: App will read urls from resources. The urls.properties file has 1000 urls
 */
public class SplitterApplication {

	private static final String EXECUTION_MODE = "manual";
	public static final String INITIAL_MSG = "\n\nWelcome to the splitter App";
	public static final String ENTER_MSG = "\n\nPlease enter a URL in the format 'scheme://domain:port/path?query_string'\n";
	public static final String CORRECTION_MSG = "Please enter a URL correct format e.g 'http://google.com:8080/search?playtech'";
	public static final String LOADING_MSG = "\n\nLoading URLs ...\n\n";

	public static void main(String[] args) {

		SplitterService splitterService = new SplitterService();

		if(args.length>0 && EXECUTION_MODE.equalsIgnoreCase(args[0])){	//Manual mode

			System.out.println(INITIAL_MSG + ENTER_MSG);
			String url;

			while (true) {
				Scanner scanner = new Scanner(System.in);
				url = scanner.nextLine();
				if (StringUtils.isEmpty(url) || !Utils.isValidUrl(url)) {
					System.out.println(CORRECTION_MSG);
				} else {
					splitterService.processUrl(url);
					System.out.println(ENTER_MSG);
				}
			}
		}
		else{		//Automatic mode

			System.out.println(INITIAL_MSG + LOADING_MSG);
			LoadingUrlsService loadingUrlsService = new LoadingUrlsService();
			List<String> urls = loadingUrlsService.readResourceFromResources();
			for (int i = 0; i < 10; ++i) {	//Do the loop 10000 times => 10 * number of urls
				urls.stream().filter(url -> !StringUtils.isEmpty(url) && Utils.isValidUrl(url))
						.forEach(url -> splitterService.processUrl(url));
			}

		}
	}
}
